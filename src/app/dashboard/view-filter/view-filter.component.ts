import { Component, OnInit } from '@angular/core';
import { ViewFilterService } from '../view-filter.service';

@Component({
  selector: 'app-view-filter',
  templateUrl: './view-filter.component.html',
  styleUrls: ['./view-filter.component.css']
})
export class ViewFilterComponent implements OnInit {

  constructor(public vfs: ViewFilterService ) { }

  ngOnInit() {
  }

}
