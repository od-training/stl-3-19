export interface View {
  age: number;
  region: string;
  date: string;
}


export interface Video {
  title: string;
  author: string;
  viewDetails: View[];
  id: string;
}

export interface Filter {
  region: string;
  from: string;
  to: string;
  minors: boolean;
  adults: boolean;
  middleAged: boolean;
  retired: boolean;
}
