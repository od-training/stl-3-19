import { TestBed } from '@angular/core/testing';

import { VideoStateService } from './video-state.service';
import { ViewFilterService } from './view-filter.service';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AppState } from '../state';

const testData = [{
  author: 'Test 1',
  id: 'abc',
  title: 'Test 1',
  viewDetails: []
}, {
  author: 'Test 2',
  id: 'def',
  title: 'Test 2',
  viewDetails: []
}];

fdescribe('VideoStateService', () => {
  let store: BehaviorSubject<AppState>;
  beforeEach(() => {
    store = new BehaviorSubject<AppState>({
      dashboard: {
        videos: testData
      }
    });
    TestBed.configureTestingModule({
      providers: [
        { provide: ViewFilterService, useValue: { filter: { valueChanges: { pipe: () => {} }}} },
        { provide: Router, useValue: {} },
        { provide: ActivatedRoute, useValue: { queryParams: { pipe: () => {} }} },
        { provide: Store, useValue: store }
      ]
    });
  });

  it('should be created', () => {
    const service: VideoStateService = TestBed.get(VideoStateService);
    expect(service).toBeTruthy();
  });

  it('should be able to retrieve a specific video from the store given an id', () => {
    const service: VideoStateService = TestBed.get(VideoStateService);
    service.getVideo('def').subscribe(v => expect(v).toBe(testData[1]));
  });

  it('should update the video when the store changes', () => {
    const service: VideoStateService = TestBed.get(VideoStateService);
    const newState: AppState = {
      dashboard: {
        videos: [
          {
            author: 'new author',
            id: 'def',
            title: 'new video',
            viewDetails: []
          }
        ]
      }
    };
    let count = 0;
    service.getVideo('def').subscribe(v => {
      if (count === 0) {
        expect(v).toBe(testData[1]);
      } else if (count === 1) {
        expect(v).toBe(newState.dashboard.videos[0]);
      }
      count++;
    });
    store.next(newState);
  });

  it('defaults to the first item in the list', () => {
    const service: VideoStateService = TestBed.get(VideoStateService);
    service.getVideo(undefined).subscribe(v => expect(v).toBe(testData[0]));
  });
});
