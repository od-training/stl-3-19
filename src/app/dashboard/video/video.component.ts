import { Component, OnInit } from '@angular/core';
import { VideoStateService } from '../video-state.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  constructor(public vss: VideoStateService) { }

  ngOnInit() {
  }

}
