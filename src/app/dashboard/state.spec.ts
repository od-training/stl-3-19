import { VideosArrivedFromServer, videosReducer, AddVideoSuccess } from './state';
import { Video } from './types';

fdescribe('dashboard reducer', () => {
  it('To accept values from the server', () => {
    const action = new VideosArrivedFromServer([{
      author: 'Test',
      id: 'abc',
      title: 'Test',
      viewDetails: []
    }]);
    const initialState = [];
    expect(videosReducer(initialState, action)).toBe(action.videos);
  });

  it('Should add a video when it succeeds on the server', () => {
    const action = new AddVideoSuccess({
      author: 'Test',
      id: 'abc',
      title: 'Test',
      viewDetails: []
    });
    const initialState: Video[] = [
      {
        author: 'Test 2',
        id: 'def',
        title: 'Test 2',
        viewDetails: []
      }
    ];
    const result = videosReducer(initialState, action);
    expect(result.length).toBe(2);
    expect(result[1]).toBe(action.video);
    expect(result).not.toBe(initialState);
  });
});
