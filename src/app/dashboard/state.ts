import { Action } from '@ngrx/store';
import { Video, View } from './types';

export const VIDEOS_ARRIVED_FROM_SERVER = 'VIDEOS_ARRIVED_FROM_SERVER';
export class VideosArrivedFromServer implements Action {
  type = VIDEOS_ARRIVED_FROM_SERVER;
  constructor(readonly videos: Video[]) { }
}

export const ADD_VIDEO = 'ADD_VIDEO';
export class AddVideo implements Action {
  type = ADD_VIDEO;
  constructor(readonly video: Partial<Video>) { }
}

export const ADD_VIDEO_SUCCESS = 'ADD_VIDEO_SUCCESS';
export class AddVideoSuccess implements Action {
  type = ADD_VIDEO_SUCCESS;
  constructor(readonly video: Video) { }
}

export const ADD_VIDEO_FAILURE = 'ADD_VIDEO_FAILURE';
export class AddVideoFailure implements Action {
  type = ADD_VIDEO_SUCCESS;
  constructor(readonly video: Video) { }
}

export const ADD_VIEW = 'ADD_VIEW';
export class AddView implements Action {
  type = ADD_VIEW;
  constructor(readonly videoId: string, readonly view: View) { }
}
export const ADD_VIEW_SUCCESS = 'ADD_VIEW_SUCCESS';
export class AddViewSuccess implements Action {
  type = ADD_VIEW_SUCCESS;
  constructor(readonly videoId: string, readonly view: View) { }
}
export const ADD_VIEW_FAILURE = 'ADD_VIEW_FAILURE';
export class AddViewFailure implements Action {
  type = ADD_VIEW_FAILURE;
  constructor(readonly videoId: string, readonly view: View) { }
}

export interface DashboardState {
  videos: Video[];
}

export function videosReducer(prev: Video[] = [], action: Action) {
  switch (action.type) {
    case VIDEOS_ARRIVED_FROM_SERVER:
      return (action as VideosArrivedFromServer).videos;
    case ADD_VIDEO_SUCCESS:
      return [...prev, (action as AddVideoSuccess).video];
    case ADD_VIEW_SUCCESS:
      const a = (action as AddViewSuccess);
      const index = prev.findIndex(v => v.id === a.videoId);
      const result = [...prev];
      result[index] = {
        ...result[index],
        viewDetails: [
          ...result[index].viewDetails,
          a.view
        ]
      };
      return result;
    default:
      return prev;
  }
}
