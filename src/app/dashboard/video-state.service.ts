import { Injectable } from '@angular/core';
import { map, switchMap, share, startWith } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { ViewFilterService } from './view-filter.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Video, Filter, View } from './types';
import { Store, select } from '@ngrx/store';
import { AppState } from '../state';
import { AddVideo, AddView } from './state';

export function filterViews(views: View[], filter: Filter) {
  return views.filter(v => filter.region === 'All' || filter.region === v.region);
}

export const selectedVideoIdQueryParam = 'id';

@Injectable({
  providedIn: 'root'
})
export class VideoStateService {
  videos = this.store.pipe(select(s => s.dashboard.videos));
  selectedVideo = this.ar.queryParams.pipe(
    map(qp => qp[selectedVideoIdQueryParam]),
    switchMap((id) => this.getVideo(id)),
    share()
  );
  filteredViews = combineLatest(this.vfs.filter.valueChanges.pipe(startWith(this.vfs.filter.value)), this.selectedVideo)
    .pipe(map(([filter, video]) => {
      return filterViews(video.viewDetails, filter);
    }));

  constructor(
    private vfs: ViewFilterService,
    private router: Router,
    private ar: ActivatedRoute,
    private store: Store<AppState>) { }

  getVideo(id: string) {
    if (id) {
      return this.videos.pipe(map(vl => vl.find(v => v.id === id)));
    } else {
      return this.videos.pipe(map(vl => vl[0]));
    }
  }

  setSelectedVideo(v: Video) {
    this.router.navigate([], {
      queryParams: {
        [selectedVideoIdQueryParam]: v.id
      },
      queryParamsHandling: 'merge'
    });
  }
  addVideo() {
    this.store.dispatch(new AddVideo({
      author: 'Auto Generated',
      title: `Computer Generated Video #${Math.ceil(Math.random() * 1000)}`,
      viewDetails: []
    }));
  }
  addView() {
    this.store.dispatch(new AddView(this.ar.snapshot.queryParams[selectedVideoIdQueryParam], {
      age: Math.ceil(Math.random() * 80),
      date: '2019-3-20',
      region: 'North America'
    }));
  }
}
