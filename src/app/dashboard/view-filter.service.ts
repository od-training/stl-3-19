import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ViewFilterService {
  filter = this.fb.group({
    region: ['All'],
    from: [''],
    to: [''],
    minors: [true],
    adults: [true],
    middleAged: [true],
    retired: [true],
  });
  regions = ['All', 'North America', 'Europe', 'Asia'];
  constructor(private fb: FormBuilder) { }
}
