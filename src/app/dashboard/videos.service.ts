import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video, View } from './types';
import { interval } from 'rxjs';
import { concatMap, startWith, shareReplay, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  constructor(private http: HttpClient) { }

  getVideos() {
    return this.http.get<Video[]>('http://localhost:8080/api/videos').pipe(shareReplay(1));
  }

  pollVideos() {
    return interval(3000).pipe(
      startWith(0),
      concatMap(() => this.getVideos())
    );
  }

  getVideo(id: string) {
    return this.http.get<Video>(`http://localhost:8080/api/videos/${id}`).pipe(shareReplay(1));
  }

  addVideo(v: Partial<Video>) {
    return this.http.post<Video>('http://localhost:8080/api/videos', v);
  }

  addView(id: string, view: View) {
    return this.getVideo(id).pipe(switchMap(video => this.http.put<Video>(`http://localhost:8080/api/videos/${id}`, {
      ...video,
      viewDetails: [
        ...video.viewDetails,
        view
      ]
    })));
  }
  deleteVideo(id: string) {
    return this.http.delete(`http://localhost:8080/api/videos/${id}`);
  }
}
