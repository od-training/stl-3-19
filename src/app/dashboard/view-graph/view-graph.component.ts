import { Component, OnInit, Input } from '@angular/core';
import { View } from '../types';

@Component({
  selector: 'app-view-graph',
  templateUrl: './view-graph.component.html',
  styleUrls: ['./view-graph.component.css']
})
export class ViewGraphComponent implements OnInit {

  @Input() views: View[];

  constructor() { }

  ngOnInit() {
  }

}
