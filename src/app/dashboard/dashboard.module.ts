import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoListComponent } from './video-list/video-list.component';
import { DashboardComponent } from './dashboard.component';
import { VideoEmbedderComponent } from './video-embedder/video-embedder.component';
import { ViewGraphComponent } from './view-graph/view-graph.component';
import { ViewFilterComponent } from './view-filter/view-filter.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { VideoComponent } from './video/video.component';
import { StoreModule } from '@ngrx/store';
import { DashboardState, videosReducer } from './state';
import { EffectsModule } from '@ngrx/effects';
import { DashboardEffectsService } from './dashboard.effects';
import { MatButtonModule, MatCardModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'video-details', component: VideoComponent }
];


@NgModule({
  declarations: [
    VideoListComponent,
    DashboardComponent,
    VideoEmbedderComponent,
    ViewGraphComponent,
    ViewFilterComponent,
    VideoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    StoreModule.forFeature<DashboardState>('dashboard', {
      videos: videosReducer
    }),
    EffectsModule.forFeature([DashboardEffectsService]),
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class DashboardModule { }
