import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { VideosService } from './videos.service';
import { map, concatMap, tap } from 'rxjs/operators';
import { VideosArrivedFromServer, ADD_VIDEO, AddVideo, AddVideoSuccess, ADD_VIEW, AddView, AddViewSuccess } from './state';

@Injectable()
export class DashboardEffectsService {
  @Effect()
  initialDataLoad = this.vs.getVideos().pipe(map(vl => new VideosArrivedFromServer(vl)));

  @Effect()
  addVideo = this.actions.pipe(
    ofType(ADD_VIDEO),
    concatMap((action: AddVideo) => this.vs.addVideo(action.video)),
    map(v => new AddVideoSuccess(v))
  );

  @Effect()
  addView = this.actions.pipe(
    ofType(ADD_VIEW),
    concatMap((action: AddView) => this.vs.addView(action.videoId, action.view)),
    map(v => new AddViewSuccess(v.id, v.viewDetails[v.viewDetails.length - 1]))
  );

  constructor(private vs: VideosService, private actions: Actions) { }
}
