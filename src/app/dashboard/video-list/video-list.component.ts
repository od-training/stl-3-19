import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  @Input() videos: Video[];
  @Input() selectedVideo: Video;
  @Output() videoClicked = new EventEmitter<Video>();

  constructor() { }

  ngOnInit() {
  }

}
