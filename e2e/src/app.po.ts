import { browser, by, element } from 'protractor';
import { VideoListPO } from './video-list.po';
import { VideoEmbedderPO } from './video-embedder.po';
import { ViewFilterPO } from './view-filter.po';
import { ViewGraphPO } from './view-graph.po';

export class AppPage {
  videoList = new VideoListPO(element(by.css('app-video-list')));
  videoEmbedder = new VideoEmbedderPO(element(by.css('app-video-embedder')));
  viewFilter = new ViewFilterPO(element(by.css('app-view-filter')));
  viewGraph = new ViewGraphPO(element(by.css('app-view-graph')));
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  addVideo() {
    return element(by.css('button[action="e2e-add-video"]')).click();
  }

  addView() {
    return element(by.css('button[action="e2e-add-view"]')).click();
  }
}
