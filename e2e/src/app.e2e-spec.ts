import { AppPage } from './app.po';
import { browser, logging } from 'protractor';
import { ViewFilterPO } from './view-filter.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be able to select a video', () => {
    page.navigateTo();
    page.videoList.getNumVideos().then(length => {
      return page.videoList.selectVideo(Math.floor(Math.random() * length));
    });
    expect(page.videoList.getSelectedVideoId()).toEqual(page.videoEmbedder.getEmbeddedVideoId());
  });

  it('should be able to filter the views', () => {
    page.navigateTo();
    page.videoList.selectVideoById('cxqRijt9LbQ');
    expect(page.viewGraph.getNumViews()).toBe(6);
    page.viewFilter.filterByRegion(ViewFilterPO.regions[1]);
    expect(page.viewGraph.getNumViews()).toBe(5);
    page.viewFilter.filterByRegion(ViewFilterPO.regions[2]);
    expect(page.viewGraph.getNumViews()).toBe(1);
    page.viewFilter.filterByRegion(ViewFilterPO.regions[3]);
    expect(page.viewGraph.getNumViews()).toBe(0);
  });

  it('should be able to add a video and views to that video', () => {
    page.navigateTo();
    const initialNumVideos = page.videoList.getNumVideos();
    page.addVideo();
    expect(initialNumVideos.then(num => num + 1)).toBe(page.videoList.getNumVideos());
    initialNumVideos.then(i => page.videoList.selectVideo(i));
    expect(page.viewGraph.getNumViews()).toBe(0);
    page.addView();
    expect(page.viewGraph.getNumViews()).toBe(1);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
