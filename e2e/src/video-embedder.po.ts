import { ElementFinder } from 'protractor';

export class VideoEmbedderPO {
  constructor(private e: ElementFinder) { }

  getEmbeddedVideoId() {
    return this.e.$('.embeded-video-id').getText();
  }
}
