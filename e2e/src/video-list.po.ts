import { ElementFinder } from 'protractor';

export class VideoListPO {
  constructor(private e: ElementFinder) { }

  selectVideo(i: number) {
    return this.e.$$('div').get(i).click();
  }

  selectVideoById(id: string) {
    return this.e.$(`div[video-id=${id}]`).click();
  }

  getSelectedVideoId() {
    return this.e.$('div.selected').getAttribute('video-id');
  }

  getNumVideos() {
    return this.e.$$('div').getWebElements().then(elements => elements.length);
  }
}
