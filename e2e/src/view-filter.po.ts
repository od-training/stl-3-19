import { ElementFinder } from 'protractor';

export class ViewFilterPO {
  static regions = [
    'All',
    'North America',
    'Europe',
    'Asia'
  ];

  constructor(private e: ElementFinder) { }

  filterByRegion(r: string) {
    return this.e.$(`input[region="${r}"]`).click();
  }
}
