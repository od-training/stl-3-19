import { ElementFinder } from 'protractor';

export class ViewGraphPO {
  constructor(private e: ElementFinder) { }

  getNumViews() {
    return this.e.$$('pre').getWebElements().then(we => we.length);
  }
}
